﻿using BenchmarkDotNet.Attributes;
using MAPZ_Lab2_Part_1_Structs.Task12;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ_Lab2_Part_2
{
    [MemoryDiagnoser]
    [RankColumn]
    public class Measurer
    {
        const int OPERATIONS_COUNT = 10_000_000;

        [Benchmark]
        public void CreateClassInstancesRegular()
        {
            PerformCalculations(IndividualTaskLogicClass.CreateClassObject);
        }

        [Benchmark]
        public void CreateClassInstancesReflection()
        {
            PerformCalculations(IndividualTaskLogicClass.CreateClassObjectReflection);
        }

        [Benchmark]
        public void InvokeMethodsRegular()
        {
            PerformCalculations(IndividualTaskLogicClass.InvokeMethod);
        }

        [Benchmark]
        public void InvokeMethodsReflection()
        {
            PerformCalculations(IndividualTaskLogicClass.InvokeMethodReflection);
        }

        private void PerformCalculations(Action action)
        {
            for (int i = 0; i < OPERATIONS_COUNT; i++)
            {
                action();
            }
        }
    }
}
