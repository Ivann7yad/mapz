﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ_Lab2_Part_1_Structs.Task12
{
    public static class IndividualTaskLogicClass
    {
        private static Type _individualTaskTestClassType;
        private static IndividualTaskTestClass _individualTaskTestClass;
        static IndividualTaskLogicClass()
        {
            _individualTaskTestClassType = typeof(IndividualTaskTestClass);
            _individualTaskTestClass = new IndividualTaskTestClass();
        }
        public static void CreateClassObject()
        {
            IndividualTaskTestClass individualTaskTestClass = new ();
        }

        public static void CreateClassObjectReflection()
        {
            IndividualTaskTestClass individualTaskTestClass = Activator.CreateInstance<IndividualTaskTestClass>();
        }

        public static void InvokeMethod()
        {
            IndividualTaskTestClass individualTaskTestClass = new();
            individualTaskTestClass.SayBeep();
        }

        public static void InvokeMethodReflection()
        {
            MethodInfo sayBeepMethod = _individualTaskTestClassType.GetMethod("SayBeep")!;
            sayBeepMethod.Invoke(_individualTaskTestClass, null);
        }
    }
}
