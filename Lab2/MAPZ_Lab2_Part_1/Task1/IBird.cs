﻿
namespace MAPZ_Lab2_Part_1.Task1
{
    public interface IBird
    {
        void Fly();
        void Chirp();
    }
}
