﻿
namespace MAPZ_Lab2_Part_1.Task1
{
    public class Swallow(string name) : Bird
    {
        public override void Chirp()
        {
            Console.WriteLine("Chirp");
        }

        public override void Fly()
        {
            Console.WriteLine("I am flying");
        }

        public override void SayHello()
        {
            Console.WriteLine($"Hello, my name is {name}");
        }
    }
}
