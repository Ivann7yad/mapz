﻿
namespace MAPZ_Lab2_Part_1.Task1
{
    public abstract class Bird : IBird
    {
        public abstract void Chirp();

        public abstract void Fly();

        public abstract void SayHello();
    }
}
