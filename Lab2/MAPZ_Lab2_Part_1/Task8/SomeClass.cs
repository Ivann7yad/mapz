﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ_Lab2_Part_1.Task8
{
    public class SomeClass
    {
        private static int _staticPrivateField = GetStaticFieldValue("private - out of constructor");
        private int _instancePrivateField = GetInstanceFieldValue("private - out of constructor");
        public static int staticPublicField = GetStaticFieldValue("public - out of constructor");
        public int instancePublicField = GetInstanceFieldValue("public - out of constructor");

        static SomeClass()
        {
            Console.WriteLine();
            Console.WriteLine("Static constructor invoked");
            _staticPrivateField = GetStaticFieldValue("private - in constructor");
            staticPublicField = GetStaticFieldValue("public - in constructor");
            Console.WriteLine();
        }
        public SomeClass()
        {
            Console.WriteLine();
            Console.WriteLine("Instance constructor invoked.");
            _instancePrivateField = GetInstanceFieldValue("private - in constructor");
            instancePublicField = GetInstanceFieldValue("public - in constructor");
            Console.WriteLine();
        }

        private static int GetInstanceFieldValue(string context)
        {
            Console.WriteLine($"GetInstanceFieldValue method called. [{context}]");
            return 1;
        }
        private static int GetStaticFieldValue(string context)
        {
            Console.WriteLine($"GetStaticFieldValue method called. [{context}]");
            return 1;
        }
    }
}
