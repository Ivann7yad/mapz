﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ_Lab2_Part_1.Task13
{
    internal struct DogStruct : IRunnable
    {
        public void Run()
        {
            Console.WriteLine("I am using all my 4 legs to run very fast");
        }

        public void Bark()
        {
            Console.WriteLine("Grrrrr, wuf, wuf");
        }
    }
}
