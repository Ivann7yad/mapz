﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ_Lab2_Part_1.Task13
{
    internal struct PersonStruct : IRunnable
    {
        public void Run()
        {
            Console.WriteLine("I am using all my 2 legs to run fast");
        }

        public void SayHello()
        {
            Console.WriteLine("Hi, I am the smartest creature on Earth");
        }
    }
}
