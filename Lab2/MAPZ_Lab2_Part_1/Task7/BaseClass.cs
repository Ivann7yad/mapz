﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ_Lab2_Part_1.Task7
{
    public class BaseClass
    {
        private string _info;
        public BaseClass() : this("Default")
        {
            
        }

        public BaseClass(string info)
        {
            this._info = info;
        }

        public void PrintInfo()
        {
            Console.WriteLine($"Info: {this._info}");
        }
    }
}
