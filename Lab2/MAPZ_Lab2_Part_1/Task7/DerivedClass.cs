﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ_Lab2_Part_1.Task7
{
    public class DerivedClass : BaseClass
    {
        private int _someNumber;
        public DerivedClass() : base()
        {
            this._someNumber = 1;
        }

        public void PrintInfo()
        {
            base.PrintInfo();
            Console.WriteLine($"Some number: {this._someNumber}");
            Console.WriteLine();
        }

        public void PrintInfo(string someName)
        {
            Console.WriteLine($"Some number: {this._someNumber}");
            Console.WriteLine($"Some name: {someName}");
            Console.WriteLine();
        }
    }
}
