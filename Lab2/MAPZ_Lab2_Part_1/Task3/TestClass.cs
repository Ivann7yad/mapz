﻿
namespace MAPZ_Lab2_Part_1.Task3
{
    class TestClass // internal.
    {
        string name; // private.
        int MyProperty { get; set; } // private.
        void Hello() { } // private.

        interface IInnerInterface { } // private.
        abstract class InnerAbstractClass { } // private.
        class InnerClass { } // private.
        struct InnerStruct { } // private.
    }
}
