﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ_Lab2_Part_1.Task11
{
    public class CastingClass
    {
        public string Name { get; set; }

        public static explicit operator string(CastingClass castingClass)
        {
            return castingClass.Name;
        }

        public static implicit operator CastingClass(string name)
        {
            var castingClass = new CastingClass();
            castingClass.Name = name;
            return castingClass;
        }
    }
}
