﻿using MAPZ_Lab2_Part_1.Task1;
using MAPZ_Lab2_Part_1.Task11;
using MAPZ_Lab2_Part_1.Task13;
using MAPZ_Lab2_Part_1.Task14;
using MAPZ_Lab2_Part_1.Task2;
using MAPZ_Lab2_Part_1.Task3;
using MAPZ_Lab2_Part_1.Task5;
using MAPZ_Lab2_Part_1.Task6;
using MAPZ_Lab2_Part_1.Task7;
using MAPZ_Lab2_Part_1.Task8;
using MAPZ_Lab2_Part_1.Task9;

#region Task1
//Swallow swallowLenny = new Swallow("Lenny");
//swallowLenny.Chirp();
//swallowLenny.SayHello();
//swallowLenny.Fly();
#endregion

#region Task2
//AccessModifiersBase accessModifiersBase = new AccessModifiersBase();
//Console.WriteLine(accessModifiersBase.ProtectedInternalName);
#endregion

#region Task3
//TestClass testClass = new TestClass();
//testClass.ToString();
//TestStructure testStructure = new TestStructure();
//testStructure.ToString();
#endregion

#region Task5
//Console.WriteLine("~Tuesday( 0000010 ): {0:b7}", (int)(~WeekDays.Tuesday));
//Console.WriteLine(" Tuesday( 0000010 ) | Wednesday( 0000100 ): {0:b7}", (int)(WeekDays.Tuesday | WeekDays.Wednesday));
//Console.WriteLine(" Tuesday( 0000010 ) & Wednesday( 0000100 ): {0:b7}", (int)(WeekDays.Tuesday & WeekDays.Wednesday));
//Console.WriteLine(" Tuesday( 0000010 ) ^ Wednesday( 0000100 ): {0:b7}", (int)(WeekDays.Tuesday ^ WeekDays.Wednesday));
#endregion

#region Task6
//Person person = new Person();
//person.Talk();
//person.Run();
#endregion

#region Task7
//DerivedClass derivedClass = new DerivedClass();
//derivedClass.PrintInfo();
//derivedClass.PrintInfo("Peter");
#endregion

#region Task8
//SomeClass someClass = new SomeClass();
#endregion

#region Task9
//int incrementedRef = 10;
//Console.WriteLine($"Before incrementing with ref: {incrementedRef}");
//RefOutDemonstrationClass.IncrementRef(ref incrementedRef);
//Console.WriteLine($"After incrementing with ref: {incrementedRef}\n");

//int initialOut = 10;
//int incrementedOut;
//Console.WriteLine($"Before incrementing with out: {initialOut}");
//RefOutDemonstrationClass.IncrementOut(initialOut, out incrementedOut);
//Console.WriteLine($"After incrementing with out: {incrementedOut}\n");

//int incrementedNoRefNoOut = 10;
//Console.WriteLine($"Before incrementing with no ref, no out: {incrementedNoRefNoOut}");
//RefOutDemonstrationClass.IncrementNoRefNoOut(incrementedNoRefNoOut);
//Console.WriteLine($"After incrementing with ref: {incrementedNoRefNoOut}\n");
#endregion

#region Task10
//double someNumber = 7.77;
//Console.WriteLine($"Some number: {someNumber}");

//object someNumberBoxed = someNumber; // Boxing.
//Console.WriteLine($"Some number boxed: {someNumberBoxed}");

//double someNumberUnboxed = (double)someNumberBoxed; // Unboxing.
//Console.WriteLine($"Some number unboxed: {someNumberUnboxed}");
#endregion

#region Task11
//CastingClass castingClass = new CastingClass();
//castingClass.Name = "Jimmy";

//string castingClassStringified = (string)castingClass;
//Console.WriteLine($"String explicit casting from CastingClass: {castingClassStringified}");

//CastingClass castingClass2 = castingClassStringified;
//Console.WriteLine($"CastingClass.Name implicit casting from string: {castingClass2.Name}");
#endregion

#region Task13
//DogStruct dog = new DogStruct();
//dog.Bark();
//dog.Run();
//Console.WriteLine();

//PersonStruct person = new PersonStruct();
//person.SayHello();
//person.Run();
#endregion

#region Task14
ObjectOverrider objectOverrider = new();
objectOverrider.Name = "Mark";
Console.WriteLine($"GetHashCode(): {objectOverrider.GetHashCode()}");
Console.WriteLine($"ToString(): {objectOverrider.ToString()}");
#endregion


