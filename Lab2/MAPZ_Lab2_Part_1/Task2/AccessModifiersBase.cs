﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ_Lab2_Part_1.Task2
{
    public class AccessModifiersBase
    {
        public string PublicName;
        protected string ProtectedName;
        private string PrivateName;

        internal string InternalName;

        private protected string PrivateProtectedName;
        
        protected internal string ProtectedInternalName;

        public AccessModifiersBase()
        {
        }
    }
}
