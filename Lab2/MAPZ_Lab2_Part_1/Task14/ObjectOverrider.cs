﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ_Lab2_Part_1.Task14
{
    public class ObjectOverrider
    {
        public string Name { get; set; } = "Some string";
        public override string ToString()
        {
            return Name;
        }

        public override bool Equals(object? obj)
        {
            if (obj is  ObjectOverrider otherObjectOverrider)
            {
                return string.Equals(this.Name, otherObjectOverrider.Name, StringComparison.CurrentCulture);
            }

            return false;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Name, DateTime.Now);
        }

        protected new ObjectOverrider MemberwiseClone()
        {
            var newEmtity = new ObjectOverrider();
            newEmtity.Name = this.Name;
            return newEmtity;
        }
    }
}
