﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ_Lab2_Part_1.Task6
{
    public interface ITalkable
    {
        void Talk();
    }
}
