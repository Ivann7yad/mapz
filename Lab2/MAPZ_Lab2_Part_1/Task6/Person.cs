﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ_Lab2_Part_1.Task6
{
    public class Person : ITalkable, IRunnable
    {
        public void Run()
        {
            Console.WriteLine("Run, Forest, run");
        }

        public void Talk()
        {
            Console.WriteLine("Talking to the moooooon");
        }
    }
}
