﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ_Lab2_Part_1_Structs.Task11
{
    public struct Castingstruct
    {
        public string Name { get; set; }

        public static explicit operator string(Castingstruct castingstruct)
        {
            return castingstruct.Name;
        }

        public static implicit operator Castingstruct(string name)
        {
            var castingstruct = new Castingstruct();
            castingstruct.Name = name;
            return castingstruct;
        }
    }
}
