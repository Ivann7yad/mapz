﻿
namespace MAPZ_Lab2_Part_1_Structs.Task3
{
    struct Teststruct // internal.
    {
        string name; // private.
        int MyProperty { get; set; } // private.
        void Hello() { } // private.

        interface IInnerInterface { } // private.
        struct InnerAbstractstruct { } // private.
        struct Innerstruct { } // private.
        struct InnerStruct { } // private.
    }
}
