﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ_Lab2_Part_1_Structs.Task5
{
    public enum WeekDays
    {
        Monday = 0b_1,
        Tuesday = 0b_10,
        Wednesday = 0b_100,
        Thursday = 0b_1000,
        Friday = 0b_10000,
        Saturday = 0b_100000,
        Sunday = 0b_1000000,
    }
}
