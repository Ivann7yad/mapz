﻿
namespace MAPZ_Lab2_Part_1_Structs.Task1
{
    public interface IBird
    {
        void Fly();
        void Chirp();
    }
}
