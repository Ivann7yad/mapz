﻿
namespace MAPZ_Lab2_Part_1_Structs.Task1
{
    public struct Swallow(string name) : IBird
    {
        public void Chirp()
        {
            Console.WriteLine("Chirp");
        }

        public void Fly()
        {
            Console.WriteLine("I am flying");
        }

        public void SayHello()
        {
            Console.WriteLine($"Hello, my name is {name}");
        }
    }
}
