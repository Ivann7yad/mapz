﻿
namespace MAPZ_Lab2_Part_1_Structs.Task1
{
    public struct Bird : IBird
    {
        public void Chirp() { }

        public void Fly() { }

        public void SayHello() { }
    }
}
