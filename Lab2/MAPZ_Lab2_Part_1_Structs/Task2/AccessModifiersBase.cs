﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ_Lab2_Part_1_Structs.Task2
{
    public interface IAccessModifiersBase
    {
        public static string PublicName;
        protected static string ProtectedName;
        private static string PrivateName;

        internal static string InternalName;

        private static protected string PrivateProtectedName;
        
        protected static internal string ProtectedInternalName;
    }
}
