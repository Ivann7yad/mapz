﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ_Lab2_Part_1_Structs.Task9
{
    public struct RefOutDemonstrationstruct
    {
        // ref.
        public static void  IncrementRef(ref int number)
        {
            number++;
        }

        // out.
        public static void IncrementOut(int initialNumber, out int incrementedNumber)
        {
            incrementedNumber = initialNumber + 1;
        }

        // no ref.
        public static void IncrementNoRefNoOut(int number)
        {
            number++;
        }

        // Following two functions are producing same result, regardless used with 'ref' or 'out'.
        public static void AssignTenRef(ref int number)
        {
            number = 10;
        }

        public static void AssignTenOut(out int number)
        {
            number = 10;
        }
    }
}
