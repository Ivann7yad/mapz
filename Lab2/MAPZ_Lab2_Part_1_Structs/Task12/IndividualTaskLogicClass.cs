﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace MAPZ_Lab2_Part_1_Structs.Task12
{
    public struct IndividualTaskLogicstruct
    {
        private Type _individualTaskTeststructType;
        public IndividualTaskLogicstruct()
        {
            _individualTaskTeststructType = typeof(IndividualTaskTeststruct);
        }
        public void CreatestructObject()
        {
            IndividualTaskTeststruct individualTaskTeststruct = new ();
        }

        public void CreatestructObjectReflection()
        {
            IndividualTaskTeststruct individualTaskTeststruct = Activator.CreateInstance<IndividualTaskTeststruct>();
        }

        public void InvokeMethod()
        {
            IndividualTaskTeststruct individualTaskTeststruct = new();
            individualTaskTeststruct.SayBeep();
        }

        public void InvokeMethodReflection()
        {
            MethodInfo sayBeepMethod = _individualTaskTeststructType.GetMethod("SayBeep")!;
            sayBeepMethod.Invoke(_individualTaskTeststructType, null);
        }
    }
}
